Introduction
============

paula.testing provides get_test_suite, that searches your package recursively,
finds all python files and registers contained unit tests for testing.
