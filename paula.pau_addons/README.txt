paula.pau_addons contains stuff that extends current PAU. Ideally this package
should be empty.

Currently it contains:

- *IPropertyInterface*, a principal carries properties as attributes. These
  are listed by schemas which are marked with IPropertyInterface. This might
  be a good or bad idea - it is here for evaluation. To my knowledge PAU
  currently has no means of putting properties on a principal, other than as
  attributes. Feedback highly appreciated.
