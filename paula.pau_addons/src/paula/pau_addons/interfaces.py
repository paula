# Copyright (c) 2008 by Florian Friesdorf
#
# GNU Affero General Public License (AGPL)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation; either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
"""
"""
__author__ = "Florian Friesdorf <flo@chaoflow.net>"
__docformat__ = "plaintext"

from zope.app.authentication.interfaces import IPluggableAuthentication
from zope.app.security.interfaces import IAuthentication

from zope.interface.interfaces import IInterface
from zope.schema import TextLine

class IPropertyInterface(IInterface):
    """Interfaces providing this are interfaces defining properties.
    """

class IPaulaAuthentication(IAuthentication, IPluggableAuthentication):
    """
    """
    def addUser(login, password):
        """Add a user to the first authenticator plugin that supports it
        """
