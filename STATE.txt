unit tests pass for:
    - paula.authentication
    - paula.authutil
    - paula.properties
    - paula.proputil
    - paula.examples
    - paula.suite
    - paula.plonepas
run from their dedicated, up-to-date zope3 buildouts, as well as from within
paula.plonepas buildout (zope-2.10.x).

paula.suite contains the integration test for:
    - PAU from zope.app.authentication with factories from principalfolder
    - paula.authentication
    - paula.authutil
    - paula.properties
    - paula.proputil
    - paula.examples
    - paula.suite
It passes in paula.suite's buildout with up-to-date zope3 eggs, as well as run
from within paula.plonepas buildout (zope-2.10.x)

paula.suite supports authentication and properties.
paula.plonepas supports authentication, properties and groups.


DONE:
    - make paula.ploneexamples content show up - thx jensens


TODO/IDEAS:
    - find nice solution for: svn:ignore <-> .gitignore
    - fix login: strangely the test seem to pass, but a real login does not
      work
    - handle id collisions between users and groups
    - register on plone.org/products
    - open bug tracker
    - check all doctest/doctexts
    - get rid of hacks
    - get rid of unnecessary code
    - group in group support
    - Code beautification
	- camelCase for functions
    - caching where necessary
    - reacting to changing objects where necessary
    - migration scripts
    - IUserAddedPlugin.doAdduser with default location
    - definition of roles on principals, i.e. IRolesPlugin.getRolesForPrincipal
      support
    - IUserEnumerationPlugin support
    - IGroupEnumerationPlugin support
    - IUserIntrospection support
	getUserIds()
	getUserNames()
	getUsers()
    - IUserManagement support
	doChangeUser - passwort
	doDeleteUser
    - IMutablePropetiesPlugin
	setPropertiesForUser
	deleteUser
    - ISchemaMutablePropertiesPlugin
	addProperty
